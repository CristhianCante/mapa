let mymap = L.map('mapid');
mymap.setView([4.627447191800487,-74.1682505607605], 16);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
miProveedor.addTo(mymap);
let micasa = L.marker([4.626965967767793, -74.1675317287445])
micasa.addTo(mymap)
let mibarrio = L.circle([4.627447191800487, -74.1682505607605], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 160
})
mibarrio.addTo(mymap);
let saloncomunal = L.polygon([
    [4.628221159766875, -74.16801854968071],
    [4.628424342926942, -74.1676390171051],
    [4.628615495451922, -74.16779190301895],
    [4.628432363313642, -74.16812717914581]
])
saloncomunal.addTo(mymap);